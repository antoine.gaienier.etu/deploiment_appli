# h-gaienier-moutte

Une entreprise de développement logiciel, vous demande d'améliorer les modes de communication à la fois des collaborateurs entre eux, mais également entre les collaborateurs et les clients de l’entreprise.

Pour cela, elle souhaite mettre en place un outil répondant aux contraintes suivantes:

- le mode de communication proposé doit supporter une conversation synchrone et asynchrone ;
    une conversation peut avoir lieu entre deux personnes (communication 1 vers 1), en petit groupe (discussion entre développeurs) ou en audience plus large (forum de discussion) ;
- les communications doivent être chiffrées de bout en bout afin d’assurer la confidentialité des échanges ;
- le service de discussion doit pouvoir être accessible en utilisant une application web, une application lourde ou une application mobile.

Après une étude des solutions existantes, le groupe de travail chargé de choisir la solution à déployer vous demande de mettre en place une solution basée sur le standard Matrix qui répond à tous les critères demandés.

### Qu'est ce que Matrix?

Matrix est un projet open source qui publie un standard pour une communication sécurisée, distribuée et temps réelle.

Un standard est l’ensemble des documentations de référence permettant à plusieurs implémentations respectant celui-ci de se comprendre. En d’autres termes, il peut y avoir plusieurs logiciels qui implémentent le même standard. Ces logiciels vont pouvoir inter-opérer car il se comprennent, parle la même langue, grâce au standard.

Le standard matrix défini la liste et le format de message permettant:

- à un serveur de discussion (back-end) de dialoguer avec un client (front-end). Ainsi, plusieurs client différents (web, mobile, application lourde, client simple en mode texte…) peuvent communiquer avec le serveur, laissant le choix de l’interface à l’utilisateur ;
- à plusieurs serveurs de discussion de fédérer leurs utilisateurs et leur canaux. Ainsi, les utilisateurs de différents fournisseurs pourront communiquer entre eux.

### A vous de mettre en place Matrix en suivant toutes les étapes qui suivent.

- ## [ Mise en place](tp01.md)

    - ### [Connexion à la machine de virtualisation](tp01.md#connexion-à-la-machine-de-virtualisation)

        - #### [Connexion 1](tp01.md#connexion-1)

        - #### [Connexion 2](tp01.md#connexion-2)

    - ### [Machine virtuelle](tp01.md#machine-virtuelle)

        - #### [Création d’une Machine virtuelle](tp01.md#création-dune-machine-virtuelle)

        - #### [ Voir la liste des Machine virtuelle](tp01.md#voir-la-liste-des-machine-virtuelle)

        - #### [Démarrage de la machine virtuelle](tp01.md#démarrage-de-la-machine-virtuelle)

        - #### [ Arrêt et suppression de la machine virtuelle](tp01.md#arrêt-et-suppression-de-la-machine-virtuelle)

        - #### [ Obtenir des informations sur la machine virtuelle](tp01.md#obtenir-des-informations-sur-la-machine-virtuelle)

        - #### [Utilisation de la machine virtuelle](tp01.md#utilisation-de-la-machine-virtuelle)

        - #### [Changement de la configuration réseau et routeur](tp01.md#changement-de-la-configuration-réseau-et-routeur)

        - #### [Changement de la configuration DNS](tp01.md#changement-de-la-configuration-dns)

    - ### [Configurer et mettre à jour la machine virtuelle](tp01.md#configurer-et-mettre-à-jour-la-machine-virtuelle)

        - #### [Connexion root et SSH](tp01.md#connexion-root-et-ssh)

        - #### [Mise à jour](tp01.md#mise-à-jour)

        - #### [Installation d’outils](tp01.md#installation-doutils)

    - ### [Quelques trucs en plus](tp01.md#quelques-trucs-en-plus)







- ## [Dernière configuration et service postgresql](tp02.md)
    - ### [Dernière configurations sur la machine viruelle](tp02.md#dernière-configurations-sur-la-machine-viruelle)

        - #### [Changement du nom de machine](tp02.md#changement-du-nom-de-machine)

        - #### [Installation et configuration de la commande sudo](tp02.md#installation-et-configuration-de-la-commande-sudo)


    - ### [Installation et configuration basique d’un serveur de base de données](tp02.md#installation-et-configuration-basique-dun-serveur-de-base-de-données)
        - #### [Installation et démarrage de Postgresql](tp02.md#installation-et-démarrage-de-postgresql)
        - #### [Créer un utlisateur, une base de données et une table sur postgresql](tp02.md#créer-un-utlisateur-une-base-de-données-et-une-table-sur-postgresql)





- ## [Instalation de synapse](tp03.md)
    - ### [Accès à un service HTTP sur la machine virtuelle](tp03.md#accès-à-un-service-http-sur-la-machine-virtuelle)

        - #### [Un premier service pour tester](tp03.md#un-premier-service-pour-tester)

        - #### [Accès au service depuis la machine physique](tp03.md#accès-au-service-depuis-la-machine-physique)


    - ### [Installation de Synapse](tp03.md#installation-de-synapse)

        - #### [ Installation du paquet sous Debian](tp03.md#installation-du-paquet-sous-debian)

        - #### [Paramétrage de l’accès à distance](tp03.md#paramétrage-de-laccès-à-distance)

    - ### [Paramétrage spécifique pour une instance dans un réseau privé](tp03.md#paramétrage-spécifique-pour-une-instance-dans-un-réseau-privé)

        - #### [Utilisation d’une base Postgres](tp03.md#utilisation-dune-base-postgres)

        - #### [Création d’utilisateurs](tp03.md#création-dutilisateurs)

        - #### [Connexion à votre serveur Matrix](tp03.md#connexion-à-votre-serveur-matrix)

        - #### [Activation de l’enregistrement des utilisateurs](tp03.md#activation-de-lenregistrement-des-utilisateurs)

    - ### [Changement de machine physique](tp03.md#changement-de-machine-physique)





- ## [Element et reverse proxy](tp04.md)

    - ### [Element Web](tp04.md#element-web)
        
        - #### [Installtion et configuration de Ngnix](tp04.md#installtion-et-configuration-de-ngnix)
        
        - #### [Redirection sur la machine physique](tp04.md#redirection-sur-la-machine-physique)
    
    - ### [Reverse proxy pour Synapse](tp04.md#reverse-proxy-pour-synapse)
        
        - #### [Introduction et choix d’un reverse proxy](tp04.md#introduction-et-choix-dun-reverse-proxy)
        
        - #### [Installation d’un reverse proxy](tp04.md#installation-dun-reverse-proxy)
   
